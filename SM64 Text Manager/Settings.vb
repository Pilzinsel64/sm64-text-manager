﻿Imports System.IO
Imports Newtonsoft.Json.Linq

Public Class Settings

    Public Property ForceUppercaseForActAndLevelNames As Boolean = True
    Public Property AutoDetectStartEndQuotationMarks As Boolean = True
    Public Property AutoFocusEditorTextBox As Boolean = False

#Region "Save/Load"

    Public Shared ReadOnly Property Instance As New Settings

    Public Shared Sub Save(filePath As String)
        File.WriteAllText(filePath, JObject.FromObject(Instance).ToString)
    End Sub

    Public Shared Sub Load(filePath As String)
        If File.Exists(filePath) Then
            _Instance = JObject.Parse(File.ReadAllText(filePath)).ToObject(Of Settings)
        Else
            _Instance = New Settings
        End If
    End Sub

#End Region

End Class
