﻿Imports System.IO
Imports System.Reflection
Imports DevComponents.DotNetBar
Imports SM64_ROM_Manager
Imports SM64Lib
Imports SM64Lib.EventArguments

Public Class FormMain

    'F i e l d s

    Private ReadOnly tabTextManager As New Tab_TextManager With {.Dock = DockStyle.Fill, .BackColor = Color.Transparent}
    Private ReadOnly controller As New TextManagerController With {.UseSettingsForOptions = False}
    Private WithEvents RomManager As RomManager = Nothing
    Private ReadOnly settingsFilePath As String = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly.Location), "Data\Settings.json")
    Private finishedLoading As Boolean = False

    'C o n s t r u c o t r s

    Public Sub New()
        'Init Files paths
        InitFilePaths()

        'Init Components
        InitializeComponent()

        'Init Controller
        AddHandler controller.RequestRomManager, Sub(e) e.RomManager = RomManager
        'AddHandler controller.RequestIsChangingTab, Sub(e) e.Value = False
        AddHandler controller.SettingOtherStatusInfo, Sub(text, foreColor) SetOtherStatusInfos(text, foreColor)
        'AddHandler controller.SettingStatusText, Sub(text) StatusText = text
        'AddHandler controller.RequestStatusText, Sub(e) e.Value = StatusText

        'Add Text Manager Tab Panel
        tabTextManager.TMController = controller
        Controls.Add(tabTextManager)
        tabTextManager.BringToFront()

        'Init App Titel
        RefreshAppTitel()

        'Load Settings
        Settings.Load(settingsFilePath)
        ButtonItem_AutoDetectStartEndQuotationMarks.Checked = Settings.Instance.AutoDetectStartEndQuotationMarks
        ButtonItem_ForceUpperCase.Checked = Settings.Instance.ForceUppercaseForActAndLevelNames
        ButtonItem_AutoFocusEditorTextBox.Checked = Settings.Instance.AutoFocusEditorTextBox

        tabTextManager.Enabled = False
        finishedLoading = True
    End Sub

    'F e a t u r e s

    Private Sub LoadSettingsToController(reloadCurrenTable As Boolean)
        controller.ForceUppercaseForActAndLevelNames = Settings.Instance.ForceUppercaseForActAndLevelNames
        controller.AutoDetectStartEndQuotationMarks = Settings.Instance.AutoDetectStartEndQuotationMarks
        controller.AutoFocusEditTextBox = Settings.Instance.AutoFocusEditorTextBox

        If finishedLoading AndAlso reloadCurrenTable Then
            tabTextManager.SetGuiForTextTable()
        End If
    End Sub

    Private Sub InitFilePaths()
        Dim config = FilePathsConfiguration.DefaultConfiguration
        config.SetFilePath("rn64crc.exe", Path.Combine(Publics.General.MyDataPath, "Tools\rn64crc.exe"))
        config.SetFilePath("Text Profiles.json", Path.Combine(Publics.General.MyDataPath, "Text Manager\Profiles.json"))
    End Sub

    Private Sub LoadRom(romfile As String)
        RomManager = New RomManager(romfile)
        RefreshAppTitel()
        controller.SendRequestReloadTextManagerLists()
        ButtonItem_Save.Enabled = True
        tabTextManager.Enabled = True
    End Sub

    Private Sub OpenRom()
        Dim ofd As New OpenFileDialog With {
            .Filter = "SM64 ROM (*.z64)|*.z64"
        }

        If ofd.ShowDialog = DialogResult.OK Then
            LoadRom(ofd.FileName)
        End If
    End Sub

    Private Sub RefreshAppTitel()
        Dim appversion As New Version(Application.ProductVersion)
        Dim versionStr As String = appversion.ToString(If(appversion.Revision <> 0, 4, If(appversion.Build <> 0, 3, 2)))
        Dim romfile As String = RomManager?.RomFile
        Dim romext As String = If(String.IsNullOrEmpty(romfile), String.Empty, $" - ""{Path.GetFileName(romfile)}""")
        Text = $"{Application.ProductName} (v{versionStr}){romext}"
    End Sub

    Private Sub SetOtherStatusInfos(text As String, foreColor As Color)
        LabelItem_OtherStatus.Text = text

        Select Case foreColor
            Case Color.Green
                foreColor = Color.LightGreen
            Case Color.Red
                foreColor = Color.DarkRed
        End Select

        LabelItem_OtherStatus.ForeColor = If(foreColor = Color.Green, Nothing, foreColor)
    End Sub

    'G u i

    Private Sub ButtonItem_Load_Click(sender As Object, e As EventArgs) Handles ButtonItem_Load.Click
        OpenRom()
    End Sub

    Private Sub ButtonItem_EditProfile_Click(sender As Object, e As EventArgs) Handles ButtonItem_EditProfile.Click
        controller.OpenTextProfileEditor()
    End Sub

    Private Sub ButtonItem_Save_Click(sender As Object, e As EventArgs) Handles ButtonItem_Save.Click
        RomManager.SaveAllTextGroups()
    End Sub

    Private Sub ButtonItem_ForceUpperCase_CheckedChanged(sender As Object, e As EventArgs) Handles ButtonItem_ForceUpperCase.CheckedChanged
        Settings.Instance.ForceUppercaseForActAndLevelNames = ButtonItem_ForceUpperCase.Checked
        LoadSettingsToController(True)
    End Sub

    Private Sub ButtonItem_AutoDetectStartEndQuotationMarks_CheckedChanged(sender As Object, e As EventArgs) Handles ButtonItem_AutoDetectStartEndQuotationMarks.CheckedChanged
        Settings.Instance.AutoDetectStartEndQuotationMarks = ButtonItem_AutoDetectStartEndQuotationMarks.Checked
        LoadSettingsToController(True)
    End Sub

    Private Sub FormMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Settings.Save(settingsFilePath)
    End Sub

    Private Sub FormMain_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        controller.SendRequestReloadTextManagerLineColors()
    End Sub

    Private Sub RomManager_GettingDefaultTextProfileInfo(rommgr As RomManager, e As GetTextProfileInfoEventArgs) Handles RomManager.GettingDefaultTextProfileInfo
        e.ProfileInfo = controller.GetDefaultTextProfileInfo
    End Sub

    Private Sub ButtonItem_AutoFocusEditorTextBox_CheckedChanged(sender As Object, e As EventArgs) Handles ButtonItem_AutoFocusEditorTextBox.CheckedChanged
        Settings.Instance.AutoFocusEditorTextBox = ButtonItem_AutoFocusEditorTextBox.Checked
        LoadSettingsToController(False)
    End Sub
End Class
