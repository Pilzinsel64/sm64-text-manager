﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormMain
    Inherits DevComponents.DotNetBar.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormMain))
        Me.Bar1 = New DevComponents.DotNetBar.Bar()
        Me.ButtonItem_Load = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_Save = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_EditProfile = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_ForceUpperCase = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_AutoDetectStartEndQuotationMarks = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem_AutoFocusEditorTextBox = New DevComponents.DotNetBar.ButtonItem()
        Me.StyleManager1 = New DevComponents.DotNetBar.StyleManager(Me.components)
        Me.Bar2 = New DevComponents.DotNetBar.Bar()
        Me.LabelItem_OtherStatus = New DevComponents.DotNetBar.LabelItem()
        Me.RibbonControl1 = New DevComponents.DotNetBar.RibbonControl()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Bar1
        '
        resources.ApplyResources(Me.Bar1, "Bar1")
        Me.Bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar
        Me.Bar1.AntiAlias = True
        Me.Bar1.IsMaximized = False
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem_Load, Me.ButtonItem_Save, Me.ButtonItem1})
        Me.Bar1.MenuBar = True
        Me.Bar1.Name = "Bar1"
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Bar1.TabStop = False
        '
        'ButtonItem_Load
        '
        resources.ApplyResources(Me.ButtonItem_Load, "ButtonItem_Load")
        Me.ButtonItem_Load.Name = "ButtonItem_Load"
        '
        'ButtonItem_Save
        '
        resources.ApplyResources(Me.ButtonItem_Save, "ButtonItem_Save")
        Me.ButtonItem_Save.Enabled = False
        Me.ButtonItem_Save.Name = "ButtonItem_Save"
        '
        'ButtonItem1
        '
        resources.ApplyResources(Me.ButtonItem1, "ButtonItem1")
        Me.ButtonItem1.BeginGroup = True
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem_EditProfile, Me.ButtonItem_ForceUpperCase, Me.ButtonItem_AutoDetectStartEndQuotationMarks, Me.ButtonItem_AutoFocusEditorTextBox})
        '
        'ButtonItem_EditProfile
        '
        resources.ApplyResources(Me.ButtonItem_EditProfile, "ButtonItem_EditProfile")
        Me.ButtonItem_EditProfile.BeginGroup = True
        Me.ButtonItem_EditProfile.Name = "ButtonItem_EditProfile"
        '
        'ButtonItem_ForceUpperCase
        '
        resources.ApplyResources(Me.ButtonItem_ForceUpperCase, "ButtonItem_ForceUpperCase")
        Me.ButtonItem_ForceUpperCase.AutoCheckOnClick = True
        Me.ButtonItem_ForceUpperCase.Checked = True
        Me.ButtonItem_ForceUpperCase.Name = "ButtonItem_ForceUpperCase"
        '
        'ButtonItem_AutoDetectStartEndQuotationMarks
        '
        resources.ApplyResources(Me.ButtonItem_AutoDetectStartEndQuotationMarks, "ButtonItem_AutoDetectStartEndQuotationMarks")
        Me.ButtonItem_AutoDetectStartEndQuotationMarks.AutoCheckOnClick = True
        Me.ButtonItem_AutoDetectStartEndQuotationMarks.Checked = True
        Me.ButtonItem_AutoDetectStartEndQuotationMarks.Name = "ButtonItem_AutoDetectStartEndQuotationMarks"
        '
        'ButtonItem_AutoFocusEditorTextBox
        '
        resources.ApplyResources(Me.ButtonItem_AutoFocusEditorTextBox, "ButtonItem_AutoFocusEditorTextBox")
        Me.ButtonItem_AutoFocusEditorTextBox.AutoCheckOnClick = True
        Me.ButtonItem_AutoFocusEditorTextBox.Name = "ButtonItem_AutoFocusEditorTextBox"
        '
        'StyleManager1
        '
        Me.StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Office2016
        Me.StyleManager1.MetroColorParameters = New DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer)), System.Drawing.Color.FromArgb(CType(CType(1, Byte), Integer), CType(CType(115, Byte), Integer), CType(CType(199, Byte), Integer)))
        '
        'Bar2
        '
        resources.ApplyResources(Me.Bar2, "Bar2")
        Me.Bar2.AntiAlias = True
        Me.Bar2.BarType = DevComponents.DotNetBar.eBarType.StatusBar
        Me.Bar2.IsMaximized = False
        Me.Bar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem_OtherStatus})
        Me.Bar2.Name = "Bar2"
        Me.Bar2.Stretch = True
        Me.Bar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Bar2.TabStop = False
        '
        'LabelItem_OtherStatus
        '
        resources.ApplyResources(Me.LabelItem_OtherStatus, "LabelItem_OtherStatus")
        Me.LabelItem_OtherStatus.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Far
        Me.LabelItem_OtherStatus.Name = "LabelItem_OtherStatus"
        '
        'RibbonControl1
        '
        resources.ApplyResources(Me.RibbonControl1, "RibbonControl1")
        Me.RibbonControl1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.RibbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonControl1.CanCustomize = False
        Me.RibbonControl1.CaptionVisible = True
        Me.RibbonControl1.ForeColor = System.Drawing.Color.Black
        Me.RibbonControl1.KeyTipsFont = New System.Drawing.Font("Tahoma", 7.0!)
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonControl1.SystemText.MaximizeRibbonText = resources.GetString("RibbonControl1.SystemText.MaximizeRibbonText")
        Me.RibbonControl1.SystemText.MinimizeRibbonText = resources.GetString("RibbonControl1.SystemText.MinimizeRibbonText")
        Me.RibbonControl1.SystemText.QatAddItemText = resources.GetString("RibbonControl1.SystemText.QatAddItemText")
        Me.RibbonControl1.SystemText.QatCustomizeMenuLabel = resources.GetString("RibbonControl1.SystemText.QatCustomizeMenuLabel")
        Me.RibbonControl1.SystemText.QatCustomizeText = resources.GetString("RibbonControl1.SystemText.QatCustomizeText")
        Me.RibbonControl1.SystemText.QatDialogAddButton = resources.GetString("RibbonControl1.SystemText.QatDialogAddButton")
        Me.RibbonControl1.SystemText.QatDialogCancelButton = resources.GetString("RibbonControl1.SystemText.QatDialogCancelButton")
        Me.RibbonControl1.SystemText.QatDialogCaption = resources.GetString("RibbonControl1.SystemText.QatDialogCaption")
        Me.RibbonControl1.SystemText.QatDialogCategoriesLabel = resources.GetString("RibbonControl1.SystemText.QatDialogCategoriesLabel")
        Me.RibbonControl1.SystemText.QatDialogOkButton = resources.GetString("RibbonControl1.SystemText.QatDialogOkButton")
        Me.RibbonControl1.SystemText.QatDialogPlacementCheckbox = resources.GetString("RibbonControl1.SystemText.QatDialogPlacementCheckbox")
        Me.RibbonControl1.SystemText.QatDialogRemoveButton = resources.GetString("RibbonControl1.SystemText.QatDialogRemoveButton")
        Me.RibbonControl1.SystemText.QatPlaceAboveRibbonText = resources.GetString("RibbonControl1.SystemText.QatPlaceAboveRibbonText")
        Me.RibbonControl1.SystemText.QatPlaceBelowRibbonText = resources.GetString("RibbonControl1.SystemText.QatPlaceBelowRibbonText")
        Me.RibbonControl1.SystemText.QatRemoveItemText = resources.GetString("RibbonControl1.SystemText.QatRemoveItemText")
        Me.RibbonControl1.TabGroupHeight = 14
        '
        'FormMain
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Bar2)
        Me.Controls.Add(Me.Bar1)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Name = "FormMain"
        Me.TopLeftCornerSize = 0
        Me.TopRightCornerSize = 0
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents ButtonItem_Load As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem_Save As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents StyleManager1 As DevComponents.DotNetBar.StyleManager
    Friend WithEvents ButtonItem_EditProfile As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Bar2 As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem_OtherStatus As DevComponents.DotNetBar.LabelItem
    Friend WithEvents RibbonControl1 As DevComponents.DotNetBar.RibbonControl
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem_ForceUpperCase As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem_AutoDetectStartEndQuotationMarks As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem_AutoFocusEditorTextBox As DevComponents.DotNetBar.ButtonItem
End Class
